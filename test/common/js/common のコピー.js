( function( $ ){

	// *	setTween *    set tween. --------------------------------------
	$.fn.setTween = function( animates, options ){
		var op = $.extend({
			"time"	:1,
			"easing"	:"linear",
			"delay"	:0,
			"onComplete":function(){}
		}, options );
		var target = $( this );
		setTimeout(function(){
			target.animate( animates, {
				duration:op.time * 700,
				easing:op.easing,
				complete:op.onComplete
			});
		}, op.delay * 700 );
		return this;
	};
})( jQuery );




$(function() {
	$(window).on('load resize', function(){
		

    var w = $(window).width();
		var h = $('body').height();
    var x_640 = 640;
    var x_1060 = 1060;

			//.eachでそれぞれのliの高さを変更
			$('.area_otumami li:not(.over_area),.area_mame li:not(.over_area)').each(function(i){
					var w_list = w - 1030;
					if (w <= 640) {
							w_list = "5em";
					} else {
						if (w_list <= 0) {
							w_list = 1;
						} else if (w_list >= 20) {
							w_list = 20;
						}
					}
					$(this).css('margin-bottom',w_list);
			});
    if (w <= x_640) {


	
		
		$('nav .menu_over').css('height',h);
		//メニュー開き
		$('#menu').click(function() {
			$('nav .pcnone').fadeIn(200, 'jswing');
			$('#menu').css('display','none');
		});
	
		//メニュー閉じ
		$('#close').click(function() {
			$('nav .pcnone').fadeOut(200, 'jswing');
			$('#menu').css('display','block');
		});
	
    //.eachでそれぞれのliの高さを変更
    $('.area_otumami li:not(.over_area),.area_mame li:not(.over_area)').each(function(i){
        $(this).css('height','auto');
    });



    } else {

			
	// PCナビ　ソーシャルボタン
	$("header>.btn_area a img").hover(function() {
		$(this).stop().animate({padding: "12px",top:"-6px",left:"-6px"}, 240);
		$(this).parent().parent().stop().animate({marginRight:"-2px"},240);
	 },function() {
		 $(this).stop().animate({padding: "6px",top:0,left:0}, 200);
		$(this).parent().parent().stop().animate({marginRight:"10px"},200);
	 });



	// 背景の枠が変動するボタン .btn_augaine
	$(".btn_augaine a").hover(function() {
		$(this).find(".btn_before").stop().animate({left: "3px",top:"2px"}, 300);
		$(this).find(".btn_after").stop().animate({left: "-4px",top:"-6px"}, 300);
	 },function() {
		$(this).find(".btn_before").stop().animate({left: "-4px",top:"-6px"}, 300);
		$(this).find(".btn_after").stop().animate({left: "3px",top:"2px"}, 300);
	 });





	// PC リストのソーシャルボタン
	$(".area_otumami li .over_area,.area_mame li .over_area").css('opacity','0');
	$(".area_otumami li,.area_mame li").hover(function() {
		$(this).find(".over_area").stop().animate({opacity: "0.9"}, 300);
		 $(this).find(".over_area p").stop().animate({top: "45%"}, 300);
	 },function() {
		 $(this).find(".over_area").stop().animate({opacity: "0"},250);
		 $(this).find(".over_area p").stop().animate({top: "100%"}, 250);
	 });
	 
	 

		// リストの高さ修正
    //一行あたりの要素数=列数を変数に格納
    var columns = 2;
    //該当するli要素の数を取得
    var liLen = $('.area_otumami li>div:not(.over_area),.area_mame li:not(.over_area)').length;
    //行の数を取得
    var lineLen = Math.ceil(liLen / columns);
    //各li要素の高さの配列
    var liHiArr = [];
    //行の高さの配列
    var lineHiArr = [];

    //liの高さを配列に格納後、それが行の中で一番高い値であれば行の高さとして格納していく繰り返し文
    for (i = 0; i <= liLen; i++) {
        liHiArr[i] = $('.area_otumami li:not(.over_area),.area_mame li:not(.over_area)').eq(i).height();
        if (lineHiArr.length <= Math.floor(i / columns) 
            || lineHiArr[Math.floor(i / columns)] < liHiArr[i]) 
        {
            lineHiArr[Math.floor(i / columns)] = liHiArr[i];
        }
    }
    
    //.eachでそれぞれのliの高さを変更
    $('.area_otumami li:not(.over_area),.area_mame li:not(.over_area)').each(function(i){
        $(this).css('height',lineHiArr[Math.floor(i / columns)] + 'px');
        $(this).find(".over_area").css('height',lineHiArr[Math.floor(i / columns)] + 'px');
    });


	 
	}//------------------------PCのみの処理
});

		// main img animation
			var	$win							= $( window ),
						$tg								= $( "#mainImg" ),
						$tgImgWrap	= $tg.find( "ul" ),
						$tgImg					= $tg.find( "li" ),
						$leftWrap			= $( "#leftImg" ),
						$rightWrap		= $( "#rightImg" ),
						$imgNum			= $tgImg.length / 2,
						nowView				= 0,
						wh								= 0,
						speed						= 2,
						changeTime		= 5,
						updwCk					= true,
						timer,
		
			getWinHeight = function(){
				wh = 484;
			},
			
			setMainImgHeight = function(){
				wh = 484;
			},
			
			move = function(){
				$leftWrap.setTween( { top:-( wh * nowView ) }, { time:speed, easing:'easeInOutCubic' } );
				$rightWrap.setTween( { bottom:-( wh * nowView ) }, { time:speed, easing:'easeInOutCubic' } );
			},
			
			moveReverse = function(){
				$leftWrap.setTween( { top:wh * nowView  }, { time:speed, easing:'easeInOutCubic' } );
				$rightWrap.setTween( { bottom:wh * nowView }, { time:speed, easing:'easeInOutCubic' } );
			},
			
			setTimer = function(){
				clearInterval( timer );
				timer = setInterval( function(){
					if( updwCk ){
						nowView++;
						move();
						if( nowView == $imgNum - 1 ) updwCk = false;
					} else {
						nowView--;
						move();
						if( nowView == 0 ) updwCk = true;
					}
				}, changeTime * 700 );
			},
			
			init = function(){
				getWinHeight();
				$tg.css( { height:wh } );
				$tgImgWrap.css( { height:wh * ( $imgNum ) } );
				$tgImg.css( { height:wh } );
				setTimer();
			}();





	// 背景の枠が変動するボタン .btn_augaine
	$(".btn_augaine a").prepend("<span class='btn_before'></span><span class='btn_after'></span>");
	
	
	//------------------------.over を透過
	$(".over") .hover(function(){
			 $(this).stop().animate({'opacity' : '0.6'}, 240); // マウスオーバーで透明度を30%にする
		},function(){
			 $(this).stop().animate({'opacity' : '1'}, 200); // マウスアウトで透明度を100%に戻す
	});


	// トップへスムーススクロール
	$('.btn_totop').click(function () {
		$('body,html').animate({
		scrollTop: 0
		}, 1000);
		// ページのトップへ 500 のスピードでスクロールする
		return false;
	});


	// リスト全体にaリンク適用
	$(".area_otumami li,.area_mame li").click(function(){
			 window.location=$(this).find("a").attr("href");
			 return false;
	});
		

		$('nav .pcnone').prepend('<div class="menu_over"></div>');
		var title_content = $('header h1').clone(true);
		var social_content = $('header .btn_area').clone(true);
		$('header nav .pcnone').prepend(title_content).append(social_content);



	　var target = $(".btn_arrow a");
	
	$(".btn_arrow a").hover(function() {
			//繰り返しの1回分を定義
			　var repeat = function(){
			　　target.find("img").stop().animate({top: "10"}, 500).animate({opacity:"0"}, 200).delay(200).animate({top: "0"},0).animate({opacity: "1"}, 50);
			　}
				
				//repeatを繰り返す
				clearInterval(repeat);
			　repeat(); //最初はすぐに実行
			　var intervaId = setInterval(repeat, 1500);
				target.find("img").mouseout(function(){
					clearInterval(intervaId);
				});
		});
	
		// ホバーすると下にアニメーションするボタン .btn_augaine
	//	$(".btn_arrow a").hover(function() {
	//    	$(this).find("img").stop().animate({top: "10"}, 500).animate({opacity:"0"}, 200).delay(200).animate({top: "0"},0).animate({opacity: "1"}, 50);
	//	 },function() {
	//    	$(this).find("img").stop().css({top: "0"}).animate({opacity: "1"}, 50);
	//	 });


});






