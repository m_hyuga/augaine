<?php
/**
 * WordPress の基本設定
 *
 * このファイルは、MySQL、テーブル接頭辞、秘密鍵、ABSPATH の設定を含みます。
 * より詳しい情報は {@link http://wpdocs.sourceforge.jp/wp-config.php_%E3%81%AE%E7%B7%A8%E9%9B%86 
 * wp-config.php の編集} を参照してください。MySQL の設定情報はホスティング先より入手できます。
 *
 * このファイルはインストール時に wp-config.php 作成ウィザードが利用します。
 * ウィザードを介さず、このファイルを "wp-config.php" という名前でコピーして直接編集し値を
 * 入力してもかまいません。
 *
 * @package WordPress
 */

// 注意: 
// Windows の "メモ帳" でこのファイルを編集しないでください !
// 問題なく使えるテキストエディタ
// (http://wpdocs.sourceforge.jp/Codex:%E8%AB%87%E8%A9%B1%E5%AE%A4 参照)
// を使用し、必ず UTF-8 の BOM なし (UTF-8N) で保存してください。

// ** MySQL 設定 - この情報はホスティング先から入手してください。 ** //
/** WordPress のためのデータベース名 */
define('DB_NAME', 'wordpress');

/** MySQL データベースのユーザー名 */
define('DB_USER', 'root');

/** MySQL データベースのパスワード */
define('DB_PASSWORD', 'Mafia001');

/** MySQL のホスト名 */
define('DB_HOST', 'localhost');

/** データベースのテーブルを作成する際のデータベースの文字セット */
define('DB_CHARSET', 'utf8');

/** データベースの照合順序 (ほとんどの場合変更する必要はありません) */
define('DB_COLLATE', '');

/**#@+
 * 認証用ユニークキー
 *
 * それぞれを異なるユニーク (一意) な文字列に変更してください。
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org の秘密鍵サービス} で自動生成することもできます。
 * 後でいつでも変更して、既存のすべての cookie を無効にできます。これにより、すべてのユーザーを強制的に再ログインさせることになります。
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '0c(kYtK[@guv3tE_*_OalLYefqoOY9b+aoz Pfg19z_!EvAo7u-rk8BKWh{[L,B;');
define('SECURE_AUTH_KEY',  '5]r|!USz4Sb#h-+ko|!X=nmA;YEQOj?bjP$Vpvu1 2P_$HY[3;?SOUw-Z{4Adu+R');
define('LOGGED_IN_KEY',    '6#U++Bbh|{yG&N3B_8SY{&3J:pZpK<a+E46x?vK;x;N5b)dIi0al/nR|bfu2p0}8');
define('NONCE_KEY',        'DuT{fQ#-c@wn8OYqxR^>=Wm{JY/|EGh,o)FLBYDmh,j<2`C=m8BG^/3q_kP%E7sj');
define('AUTH_SALT',        's+bida8?W~Vp<OYdR9e8*&IDb`^e`:j_l7ns0RB*B|qd|<k;(y86K2#QQ.(,J0@7');
define('SECURE_AUTH_SALT', '}jw=/uu)b+JywSJ|]s,D^P4QbN+qCC8IBiR=J(yb9.zePBM!R3SSe]+n2QvD8?5S');
define('LOGGED_IN_SALT',   'd*]d>LRnG+D|u$T-^ko?p5[@I.NK/*m|>/vHcs<a&HL!r9KI#[$:~ePBW&R] hc!');
define('NONCE_SALT',       'lP?Kc&y4}Y=){Qqe*~Q}tMnDZ:Oy8uee_:qB6^gs`mDv.oJq+TU#_tVL+1:xrO-4');

/**#@-*/

/**
 * WordPress データベーステーブルの接頭辞
 *
 * それぞれにユニーク (一意) な接頭辞を与えることで一つのデータベースに複数の WordPress を
 * インストールすることができます。半角英数字と下線のみを使用してください。
 */
$table_prefix  = 'wp_';

/**
 * 開発者へ: WordPress デバッグモード
 *
 * この値を true にすると、開発中に注意 (notice) を表示します。
 * テーマおよびプラグインの開発者には、その開発環境においてこの WP_DEBUG を使用することを強く推奨します。
 */
define('WP_DEBUG', false);

/* 編集が必要なのはここまでです ! WordPress でブログをお楽しみください。 */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

